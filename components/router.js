const routerComp = {
  props: ['active'],
  template: `
  <div>

    <div class="page-home" v-if="active===0">
      <highlighted-comp></highlighted-comp>
      <articles-comp></articles-comp>
    </div>

    <div class="page-about" v-if="active===1">
      <div class="wrapper">
        Page About
      </div>
    </div>

    <div class="page-blog" v-if="active===2">
      <articles-comp></articles-comp>
    </div>
    
    <div class="page-contact" v-if="active===3">
      <div class="wrapper">
        Page Contact
      </div>
    </div>

    <div class="page-detail" v-if="active==='detail'">
      <detail-comp></detail-comp>
    </div>

  </div>`
}