const headerComp = {
  data() {
    return { 
      subcatsOpened: false,
      responseNavigOpened: false,
      responseSubcatsOpened: false, 
    }
  },
  props: ['items'],
  inject: ['activePage', 'activeCategory', 'categories'],
  methods: {
    switchResponseNavig() {
      this.responseNavigOpened = !this.responseNavigOpened
    },
    switchSubcats(state) {
      this.subcatsOpened = state
      this.responseSubcatsOpened = state
    },
    pageSwitch(index) {
      document.dispatchEvent(new CustomEvent('onPageSwitch', { detail: { index: index }}))
    },
    selectCategory(index) {
      document.dispatchEvent(new CustomEvent('onSelectCategory', { detail: { index: index }}))
    }
  },
  created() {
    document.addEventListener('click', (ev) => { 
      if(
        !ev.target.classList.contains('subcat') &&
        !ev.target.classList.contains('opener') &&
        !ev.target.classList.contains('openericon')
      ) { this.subcatsOpened = false; }
      
      if (
        !ev.target.classList.contains('hamburger') &&
        !ev.target.classList.contains('opener') &&
        !ev.target.classList.contains('openericon') &&
        !ev.target.classList.contains('navig') &&
        !ev.target.classList.contains('main')
      ) { 
        this.responseNavigOpened = false
        this.responseSubcatsOpened = false
      }
    })
  },
  template: `<div class="header wrapper">
    <h1><a href=""><img src="logo.png" alt="logo"/></a></h1>
    <div class="navig">
      <div class="item" :class="activePage===index ? 'active' : ''" 
        v-for="(item, index) in items" :key="index"
      >
        <div class="main">
          <a href="#" class="itemtitle" @click="pageSwitch(index)">{{ item.title }}</a>
          <a href="#" class="opener" v-if="item.subcats" @click="switchSubcats(!subcatsOpened)">
            <span class="openericon">&#9660;</span>
          </a>
          <div class="subcats" v-if="item.subcats && subcatsOpened">
            <a href="#" class="subcat" @click="selectCategory(sind)"
              v-for="(subcat, sind) in categories" :key="sind" 
            >{{ subcat }}</a>
          </div>
        </div>
      </div>
    </div>

    <div class="response">
      <button class="hamburger" @click="switchResponseNavig()">&#9776;</button>
      <div class="navig" v-if="responseNavigOpened">
        <div class="item" :class="activePage===index ? 'active' : ''" 
          v-for="(item, index) in items" :key="index"
        >
          <div class="main">
            <a href="#" class="itemtitle" @click="pageSwitch(index)">{{ item.title }}</a>
            <a href="#" class="opener" v-if="item.subcats" @click="switchSubcats(!subcatsOpened)">
              <span class="openericon">&#9660;</span>
            </a>
            <div class="subcats" v-if="item.subcats && responseSubcatsOpened">
              <a href="#" class="subcat" @click="selectCategory(sind)"
                v-for="(subcat, sind) in categories" :key="sind"
              >{{ subcat }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>`
}