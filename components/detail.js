const detailComp = {
  inject: ['detail'],
  template: `<div class="detail">
      <h2 class="title">
        <div class="wrapper">{{ detail.title }}</div>
      </h2>
      <div class="content wrapper-small" v-html="detail.content"></div>
    </div>
  `
}