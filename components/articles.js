const articlesComp = {
  data() {
    return { docWidth: 0 }
  },
  inject: ['activePage', 'activeCategory', 'categories', 'articles'],
  computed: {
    filteredArticles() {
      let arts = this.articles;
      if (this.activePage === 0) { arts = arts.filter(a => a.highlighted === false) }
      if (this.activeCategory !== null) { arts = arts.filter(a => a.category === this.activeCategory) }
      return arts;
    }
  },
  methods: {
    openDetail(id) {
      document.dispatchEvent(new CustomEvent('onOpenDetail', { detail: { id: id }}))
    },
    selectCategory(index) {
      document.dispatchEvent(new CustomEvent('onSelectCategory', { detail: { index: index }}))
    },
    getSrcset(item) {
      return item.thumb + ' 400w, ' + item.thumbSmall + ' 250w';
    },
    onImgLoad() { 
      var elem = document.querySelector('.grid');
      // refresh Masonry after each image load like this?
      var msnry = new Masonry(elem, {
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer',
        percentPosition: true
      });      
    },
    trimTitle(str) {
      let maxlength = 55;
      if (str.length > maxlength) { return str.substr(0, maxlength) + '...' }
      else { return str }
    },
  },
  created() {
    this.docWidth = document.body.clientWidth;
  },
  updated() {
    var elem = document.querySelector('.grid');
    // refresh Masonry after category selection
    var msnry = new Masonry(elem, {
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      gutter: '.gutter-sizer',
      percentPosition: false
    });  
  },
  template: `<div class="articles wrapper">
    <div class="grid">
      <div class="grid-sizer"></div>
      <div class="gutter-sizer"></div>
      <div class="grid-item" v-for="item in filteredArticles" :key="item.id">
        <img :src="item.thumb" :srcset="getSrcset(item)" @load="onImgLoad()"/>
        <a class="category" href="#" @click="selectCategory(item.category)">{{ categories[item.category] }}</a>
        <div class="title" @click="openDetail(item.id)">{{ trimTitle(item.title) }}</div>
        <span class="divider"></span>
        <div class="perex">{{ item.perex }}</div>
        <a class="more" href="#" @click="openDetail(item.id)">Read More</a>
      </div>
    </div>
  </div>`
}