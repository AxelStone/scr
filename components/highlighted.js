const highlightedComp = {
  data() {
    return { docWidth: 0 }
  },
  inject: ['activePage', 'articles', 'categories'],
  computed: {
    filteredArticles() { return this.articles.filter(a => a.highlighted) }
  },
  methods: {
    openDetail(id) {
      document.dispatchEvent(new CustomEvent('onOpenDetail', { detail: { id: id }}))
    },
    selectCategory(index) {
      document.dispatchEvent(new CustomEvent('onSelectCategory', { detail: { index: index }}))
    },
    trimTitle(str) {
      let maxlength = 55;
      if (this.docWidth < 1500 && this.docWidth > 1200 ) { maxlength = 50 } 
      if (this.docWidth < 1200) { maxlength = 45 } 
      if (str.length > maxlength) { return str.substr(0, maxlength) + '...' }
      else { return str }
    },
    getBgImage(item) {
      return this.docWidth > 1200 ? 'background-image: url('+item.thumb+');' : 'background-image: url('+item.thumbSmall+');'
    },
    getSmallImage(item) {
      return this.docWidth > 1200 ? item.thumb : item.thumbSmall;
    }
  },
  created() {
    this.docWidth = document.body.clientWidth;
  },
  template: `<div class="highlighted">
    <div class="item" :style="getBgImage(item)"
      v-for="item in filteredArticles" :key="item.id"
    >
      <div class="thumb">
        <img :src="getSmallImage(item)" alt="Placeholder image"/>
      </div>
      <div class="content">
        <a class="category" href="#" @click="selectCategory(item.category)">{{ categories[item.category] }}</a>
        <div class="title" @click="openDetail(item.id)">{{ trimTitle(item.title) }}</div>
        <div class="perex">{{ item.perex }}</div>
        <a class="more" href="#" @click="openDetail(item.id)">Read More</a>
      </div>
    </div>
  </div>`
}