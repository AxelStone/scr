const dummyPerex1 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
const dummyPerex2 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.'

const dummyContent = `
  <img width="100%" height="auto"
    srcset="https://via.placeholder.com/800x300.png 800w, https://via.placeholder.com/600x200.png 600w, https://via.placeholder.com/400x150.png 400w"
    src="https://via.placeholder.com/800x200.png" alt="Placeholder image"
  >
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  
  <blockquote>Reload page after window resize in order to load appropriate image resolutions and trim too long title length in thumbnails correctly!</blockquote>
  
  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>

  <img width="100%" height="auto"
    srcset="https://via.placeholder.com/800x500.png/abd/78a 800w, https://via.placeholder.com/600x400.png/abd/78a 600w, https://via.placeholder.com/400x300.png/abd/78a 400w"
    src="https://via.placeholder.com/800x500.png/abd/78a" alt="Placeholder image"
  >

  <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
`;

const consts = {
  navigation: [
    { title: 'Home' }, 
    { title: 'About' }, 
    { title: 'Blog', subcats: true }, 
    { title: 'Contact' }
  ],
  categories: ['lifestyle', 'travel', 'science', 'career', 'health'],
  articles: [
    { id: 0,  category: 0, highlighted: true,  content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x520.png/44b/007', thumbSmall: 'https://via.placeholder.com/260x300.png/44b/007', title: 'Short title' },
    { id: 1,  category: 1, highlighted: true,  content: dummyContent, perex: dummyPerex2, thumb: 'https://via.placeholder.com/440x580.png/b44/700', thumbSmall: 'https://via.placeholder.com/260x330.png/b44/700', title: 'And this is slightly longer article title' },
    { id: 2,  category: 0, highlighted: true,  content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x520.png/4a4/070', thumbSmall: 'https://via.placeholder.com/260x300.png/4a4/070', title: 'But what about super long article title that must be trimmed in order to fit into the thumbnail image?' },
    { id: 3,  category: 4, highlighted: true,  content: dummyContent, perex: dummyPerex2, thumb: 'https://via.placeholder.com/440x580.png/da0/950', thumbSmall: 'https://via.placeholder.com/260x330.png/da0/950', title: 'Refresh after resize' },
    { id: 4,  category: 2, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x300.png', thumbSmall: 'https://via.placeholder.com/260x180.png', title: 'Article title' },
    { id: 5,  category: 3, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x440.png', thumbSmall: 'https://via.placeholder.com/260x220.png', title: 'Article title' },
    { id: 6,  category: 2, highlighted: false, content: dummyContent, perex: dummyPerex2, thumb: 'https://via.placeholder.com/440x300.png', thumbSmall: 'https://via.placeholder.com/260x200.png', title: 'Article title' },
    { id: 7,  category: 0, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x440.png', thumbSmall: 'https://via.placeholder.com/260x170.png', title: 'Article title' },
    { id: 8,  category: 3, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x300.png', thumbSmall: 'https://via.placeholder.com/260x260.png', title: 'Article title' },
    { id: 9,  category: 4, highlighted: false, content: dummyContent, perex: dummyPerex2, thumb: 'https://via.placeholder.com/440x400.png', thumbSmall: 'https://via.placeholder.com/260x200.png', title: 'Article title' },
    { id: 10, category: 1, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x520.png', thumbSmall: 'https://via.placeholder.com/260x280.png', title: 'Article title' },
    { id: 11, category: 3, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x300.png', thumbSmall: 'https://via.placeholder.com/260x200.png', title: 'Article title' },
    { id: 12, category: 4, highlighted: false, content: dummyContent, perex: dummyPerex1, thumb: 'https://via.placeholder.com/440x520.png', thumbSmall: 'https://via.placeholder.com/260x280.png', title: 'Article title' }
  ]
}

