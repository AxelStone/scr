const root = {
  data: () => {
    return {
      activePage: 0,
      activeCategory: null,
      navigation: consts.navigation,
      categories: consts.categories,
      articles: consts.articles,
      detail: null
    }
  },

  provide() {
    return {
      activePage: Vue.computed(() => this.activePage),
      activeCategory: Vue.computed(() => this.activeCategory),
      categories: this.categories,
      articles: this.articles,
      detail: Vue.computed(() => this.detail)
    }
  },

  methods: {
    pageSwitch(page) {
      this.activePage = page;
      this.activeCategory = null;
      this.detail = null;
    },
    selectCategory(index) {
      this.activePage = 2;
      this.activeCategory = index;
      this.detail = null;
    },
    openDetail(id) { 
      this.activePage = 'detail',
      this.detail = this.articles.find(a => a.id === id)
    }
  },

  created() {
    document.addEventListener('onPageSwitch', (ev) => { 
      this.pageSwitch(ev.detail.index)
    })
    document.addEventListener('onSelectCategory', (ev) => {
      this.selectCategory(ev.detail.index)
    })
    document.addEventListener('onOpenDetail', (ev) => {
      this.openDetail(ev.detail.id)
    })
  }
}

const app = Vue.createApp(root)
app.config.unwrapInjectedRef = true

app.component('header-comp', headerComp)
app.component('router-comp', routerComp)
app.component('highlighted-comp', highlightedComp)
app.component('articles-comp', articlesComp)
app.component('detail-comp', detailComp)

app.mount('#vue-app')